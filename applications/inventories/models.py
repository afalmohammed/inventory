from django.db import models
from django.utils.translation import ugettext as _
from django.db.models.signals import post_save
from django.dispatch import receiver
from applications.accounts.models import User, Teams
from applications.accounts.models import TimeStampModel


class BinLocation(TimeStampModel):
    name = models.CharField(max_length=256, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Bin Location')
        verbose_name_plural = _('Bin Location')
        ordering = ('-created',)


class Projects(TimeStampModel):
    name = models.CharField(max_length=256, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Projects')
        verbose_name_plural = _('Projects')
        ordering = ('-created',)


class Shelves(TimeStampModel):
    name = models.CharField(max_length=256, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Shelves')
        verbose_name_plural = _('Shelves')
        ordering = ('-created',)


class Innerholes(TimeStampModel):
    name = models.CharField(max_length=256, blank=True, null=True)
    bin_obj = models.ForeignKey(BinLocation, related_name="bin_hole", null=True, blank=True, on_delete=models.CASCADE)
    shelf = models.ForeignKey(Shelves, related_name="shelf_obj", null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Inner holes')
        verbose_name_plural = _('Inner holes')
        ordering = ('-created',)


class Vendors(TimeStampModel):
    name = models.CharField(max_length=256, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Vendors')
        verbose_name_plural = _('Vendors')
        ordering = ('-created',)


class Product(TimeStampModel):
    part_description = models.CharField(max_length=256, null=True, blank=True)
    part_name = models.CharField(max_length=256, null=True, blank=True)
    remarks = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.part_description

    class Meta:
        verbose_name = _('Products')
        verbose_name_plural = _('Products')
        ordering = ('-created',)


class ProductPurchase(TimeStampModel):
    product_purchase = models.ForeignKey(Product, related_name="product_purchase", null=True, blank=True,
                                         on_delete=models.CASCADE)
    purchased_on = models.DateField(null=True, blank=True)
    purchaser = models.ForeignKey(User, related_name="purchase", null=True, blank=True, on_delete=models.CASCADE)
    vendor_name = models.ForeignKey(Vendors, related_name="vendor", null=True, blank=True, on_delete=models.CASCADE)
    purchase_count = models.IntegerField(null=True, blank=True, default=0)
    location = models.ForeignKey(Innerholes, related_name="hole", null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.product_purchase.part_description

    class Meta:
        verbose_name = _('Product Purchase')
        verbose_name_plural = _('Product Purchase')
        ordering = ('-created',)


class OverallInventory(TimeStampModel):
    product_overall = models.ForeignKey(Product, related_name="overall_product", null=True, blank=True,
                                        on_delete=models.CASCADE)
    product_purchased_count = models.IntegerField(null=True, blank=True, default=0)
    product_out_count = models.IntegerField(null=True, blank=True, default=0)
    product_returned_count = models.IntegerField(null=True, blank=True, default=0)
    total_count = models.IntegerField(null=True, blank=True, default=0)
    location = models.ForeignKey(Innerholes, related_name='overall_location', blank=True, null=True,
                                 on_delete=models.CASCADE)

    def __str__(self):
        return self.product_overall.part_description

    class Meta:
        verbose_name = _('Overall Inventory')
        verbose_name_plural = _('Overall Inventory')
        ordering = ('-created',)


class File(TimeStampModel):
    xl_file = models.FileField(upload_to="Excel Files", blank=True, null=True)


CHOICES = (
    ("1", 'Pending'),
    ("2", 'Order Placed'),
    ("3", 'Delivered'),
   )

CHOICES_2 = (
    ("1", 'Pending'),
    ("2", 'In-Progress'),
    ("3", 'Delivered'),
   )

CHOICES_3 = (
    ("1", 'Yes'),
    ("2", 'No'),
   )

CHOICES_4 = (
    ("Act-1", 'USB'),
    ("Act-2", 'TypeC'),
    ("Act-3", 'HDMI'),
    ("Act-4", 'DP'),
    ("Act-5", '3.5mm'),
    ("Act-6", 'TypeC'),
   )

def number():
    no = PurchaseRequest.objects.count()
    if no == 0:
        return 1
    else:
        return no + 1


class PurchaseRequest(TimeStampModel):
    product_purchase = models.ForeignKey(Product, related_name="purchase_product", null=True, blank=True,
                                         on_delete=models.CASCADE)
    requested_by = models.CharField(max_length=256, null=True, blank=True)
    request_number = models.IntegerField(null=True, blank=True, default=number)
    requested_on = models.DateField(null=True, blank=True)
    status = models.CharField(max_length=2, null=True, blank=True, choices=CHOICES)
    age = models.IntegerField(null=True, blank=True)
    project = models.ForeignKey(Projects, related_name="request_project", on_delete=models.CASCADE,
                                null=True, blank=True)
    purchaser = models.ForeignKey(User, related_name="purchase_user", on_delete=models.CASCADE, null=True, blank=True)
    count = models.IntegerField(null=True, blank=True, default=0)

    def __str__(self):
        return self.requested_by + " " + self. product_purchase.part_description

    class Meta:
        verbose_name = _('Purchase Request')
        verbose_name_plural = _('Purchase Requests')
        ordering = ('-created',)


class ModelNames(TimeStampModel):
    model_name = models.CharField(max_length=256, null=True, blank=True)
    voltage = models.CharField(max_length=256, null=True, blank=True)
    item_code = models.CharField(max_length=20, null=True, blank=True)
    count_delivered = models.IntegerField(null=True, blank=True)
    date_of_manufacture = models.DateField(null=True, blank=True)
    remarks = models.CharField(max_length=256, null=True, blank=True)

    def __str__(self):
        return self.model_name

    class Meta:
        verbose_name = _('Model Names')
        verbose_name_plural = _('Model Names ')
        ordering = ('-created',)


class TopPlates(TimeStampModel):
    name = models.CharField(max_length=256, null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Top Plate')
        verbose_name_plural = _('Top Plates ')
        ordering = ('-created',)


class WorkWeek(TimeStampModel):
    work_week = models.CharField(max_length=25, null=True, blank=True)

    def __str__(self):
        return self.work_week

    class Meta:
        verbose_name = _('Work Week')
        verbose_name_plural = _('Work Weeks')
        ordering = ('-created',)


class PurchaseOrder(TimeStampModel):
    order_no = models.CharField(max_length=256, null=True, blank=True)
    ordered_count = models.IntegerField(null=True, blank=True)
    destination = models.CharField(max_length=256, null=True, blank=True)
    device = models.ForeignKey(Projects, related_name="oredered_device", null=True, blank=True,
                                on_delete=models.CASCADE)
    total_delivered = models.IntegerField(null=True, blank=True)
    placed_on = models.DateField(null=True, blank=True)
    delivery_status = models.CharField(max_length=2, null=True, blank=True, choices=CHOICES_2, default='2')


    def __str__(self):
        # return self.order_no + " for " + str(self.ordered_count) + " number of " + self.device.name + " to " + \
        #        self.destination
        return str(self.order_no)

    class Meta:
        verbose_name = _('Purchase Order')
        verbose_name_plural = _('Purchase Orders ')
        ordering = ('-created',)


class MachineSerialNumber(TimeStampModel):
    serial_no = models.IntegerField(null=True, blank=True)
    po_number = models.ForeignKey(PurchaseOrder, related_name="purchase_order", on_delete=models.CASCADE,
                                  null=True, blank=True)
    default_top_plate = models.ForeignKey(TopPlates,related_name='plate', blank=True, null=True,
                                          on_delete=models.CASCADE)
    extra_top_plate = models.ForeignKey(TopPlates, null=True, blank=True, related_name="extra_plate",
                                        on_delete=models.CASCADE)
    delivery_work_week = models.ForeignKey(WorkWeek, related_name="weeks", null=True, blank=True,
                                           on_delete=models.CASCADE)
    delivery_status = models.CharField(max_length=2, null=True, blank=True, choices=CHOICES_2, default='1')
    invoiced = models.CharField(max_length=2, null=True, blank=True, choices=CHOICES_3, default='2')

    def __str__(self):
        # return str(self.serial_no) + " of order number " + str(self.po_number.order_no) + \
        #        "(" + str(self.po_number.ordered_count) + ")"
        return str(self.serial_no)

    class Meta:
        verbose_name = _('Machine number')
        verbose_name_plural = _('Machine Numbers')
        ordering = ('-created',)


class Accessories(TimeStampModel):
    name = models.CharField(max_length=256, null=True, blank=True)
    item_code = models.CharField(max_length=20, null=True, blank=True)
    model_names = models.ManyToManyField(ModelNames, related_name='models', blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Accessories')
        verbose_name_plural = _('Accessories')
        ordering = ('-created',)


class ProductOut(TimeStampModel):
    product_out = models.ForeignKey(Product, related_name="inventory_out", null=True, blank=True,
                                    on_delete=models.CASCADE)
    recieved_date = models.DateField(null=True, blank=True)
    project = models.ForeignKey(Projects, related_name="project", null=True, blank=True, on_delete=models.CASCADE)
    team = models.ForeignKey(Teams, related_name="team", null=True, blank=True, on_delete=models.CASCADE)
    return_count = models.IntegerField(null=True, blank=True, default=0)
    person = models.ForeignKey(User, related_name="allocated_person", null=True, blank=True, on_delete=models.CASCADE)
    out_count = models.IntegerField(null=True, blank=True, default=0)
    purchase_order_number = models.ForeignKey(PurchaseOrder, related_name="po_number", null=True, blank=True,
                                        on_delete=models.CASCADE)
    machine_number = models.ForeignKey(MachineSerialNumber, related_name="sl_number", null=True, blank=True,
                                        on_delete=models.CASCADE)

    def __str__(self):
        return self.product_out.part_description

    class Meta:
        verbose_name = _('Product Out')
        verbose_name_plural = _('Product Out')
        ordering = ('-created',)


class ProjectInventory(TimeStampModel):
    inventory = models.ForeignKey(Product, related_name="project_item", null=True, blank=True, on_delete=models.CASCADE)
    project = models.ManyToManyField(Projects, related_name="project_name", blank=True)
    available = models.ForeignKey(OverallInventory, related_name="overall", null=True, blank=True,
                                  on_delete=models.CASCADE)
    required_count = models.IntegerField(null=True, blank=True, default=0)
    product_out = models.ForeignKey(ProductOut, related_name="out_product", null=True, blank=True,
                                    on_delete=models.SET_NULL)

    def __str__(self):
        return self.inventory.part_description

    class Meta:
        verbose_name = _('Project BOM')
        verbose_name_plural = _('Project BOM')
        ordering = ('-created',)



class InventoryReturned(TimeStampModel):
    product_returned = models.ForeignKey(Product, related_name="product_returned", null=True, blank=True,
                                         on_delete=models.CASCADE)
    count_returned = models.IntegerField(null=True, blank=True, default=0)
    returned_by = models.CharField(max_length=256, null=True, blank=True)
    returned_date = models.DateField(null=True, blank=True)
    project = models.ForeignKey(Projects, related_name="project_return", null=True, blank=True,
                                on_delete=models.CASCADE)
    team = models.ForeignKey(Teams, related_name="team_return", null=True, blank=True, on_delete=models.CASCADE)
    person = models.ForeignKey(User, related_name="returned_person", null=True, blank=True, on_delete=models.CASCADE)
    out_object = models.ForeignKey(ProductOut, related_name="out_inventory", null=True, blank=True,
                                   on_delete=models.CASCADE)

    def __str__(self):
        return self.product_returned.part_description

    class Meta:
        verbose_name = _('Returned Inventory')
        verbose_name_plural = _('Returned Inventory')
        ordering = ('-created',)

class Actuators(TimeStampModel):
    serial_num = models.IntegerField(null=True, blank=True)
    type = models.CharField(max_length=30, null=True, blank=True)
    po_number = models.ForeignKey(PurchaseOrder, related_name="actuator_po", null=True, blank=True,
                                  on_delete=models.CASCADE)
    machine_no = models.ForeignKey(MachineSerialNumber, related_name="machine_num", null=True, blank=True,
                                  on_delete=models.CASCADE)
    count_delivered = models.IntegerField(null=True, blank=True)


    def __str__(self):
        return self.type

    class Meta:
        verbose_name = _('Actuator')
        verbose_name_plural = _('Actuators')
        ordering = ('-created',)


class AccessoryDelivered(TimeStampModel):
    accessory = models.ForeignKey(Accessories, related_name="delivered_accessory", null=True, blank=True,
                                   on_delete=models.CASCADE)
    purchase_number = models.ForeignKey(PurchaseOrder, related_name="order_number", on_delete=models.CASCADE, null=True,
                                        blank=True)
    machine_serial_number = models.ForeignKey(MachineSerialNumber, related_name="serial_num", on_delete=models.CASCADE,
                                              null=True, blank=True)
    count_delivered = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.accessory.name

    class Meta:
        verbose_name = _('Accessory Delivered')
        verbose_name_plural = _('Accessories Delivered')
        ordering = ('-created',)

# @receiver(post_save, sender=OverallInventory)
# def my_callback(sender, instance, *args, **kwargs):
#     instance.total_count = instance.product_out_count + instance.product_purchased_count +\
#                            instance.product_returned_count


# @receiver(post_save, sender=PurchaseRequest)
# def user_post_save(sender, instance, **kwargs):
#     if kwargs['created']:
#         message = render_to_string('request_confirmation_mail.html', {
#             'today': datetime.today().strftime('%Y-%m-%d'),
#             'part_name': instance.product_purchase.part_name,
#             'part_description': instance.product_purchase.part_description,
#             'count': instance.count,
#             'request_no': instance.request_number,
#             "requested_on": instance.requested_on,
#             "status": instance.get_status_display(),
#             "project": instance.project,
#             "requested_by": instance.requested_by,
#             "purchaser": instance.purchaser.first_name
#         })
#         print(instance.purchaser.first_name)
#         to_email = [instance.requested_by, instance.purchaser.email]
#         send_mail(
#             'New Request Placed.',
#             'RandD Inventory',
#             'inventory.ustglobal@gmail.com',
#             to_email,
#             html_message=message, )
#     else:
#         pass


# @receiver(post_save, sender=ProductPurchase)
# def save_overall(sender, instance, **kwargs):
#     # try:
#     # obj = OverallInventory.objects.get(product_purchase=instance.product_purchase)
#     # except:
#     obj = OverallInventory()
#     obj.product_overall = instance.product_purchase
#     total_purchase_count = obj.product_purchased_count + instance.purchase_count
#     obj.total_count = total_purchase_count - obj.product_out_count + obj.product_returned_count
#     obj.save()


