import json
import xlwt
from datetime import datetime
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from django.utils.decorators import method_decorator
from django.views import View
from .models import Product, ProductOut, ProductPurchase, OverallInventory, BinLocation, InventoryReturned, \
    PurchaseRequest, Projects, Vendors, ProjectInventory, Shelves, Innerholes, ModelNames, Accessories, \
    PurchaseOrder, WorkWeek, Actuators, TopPlates, MachineSerialNumber, CHOICES, CHOICES_2, CHOICES_3, CHOICES_4, \
    AccessoryDelivered
from applications.accounts.models import User, Teams

CHOICES = (
            ("1", 'Pending'),
            ("2", 'Order Placed'),
            ("3", 'Delivered'),
        )


def validate_mail(email):
    try:
        validate_email(email)
        return True
    except ValidationError:
        return False


class DashboardView(View):

    @method_decorator(login_required)
    def get(self, request):
        return render(request, 'index.html')


class ProductOutView(View):
    @method_decorator(login_required)
    def get(self, request):
        today = datetime.today().date()
        items_list = ProductOut.objects.all()
        overall = OverallInventory.objects.all()
        orders = PurchaseOrder.objects.all()
        machines = MachineSerialNumber.objects.all()
        teams = Teams.objects.all()
        users = User.objects.all()
        mechanical_users = users.filter(team__name="Mechanical")
        electronics_users = users.filter(team__name="Electronics")
        software_users = users.filter(team__name="Software")
        production_users = users.filter(team__name="Production")
        context = {
            'items_list': items_list,
            'overall': overall,
            'today': today,
            'teams': teams,
            'orders': orders,
            "machines": machines,
            "mechanical_users": mechanical_users,
            "electronics_users": electronics_users,
            "software_users": software_users,
            "production_users": production_users
        }
        return render(request, 'new_product_out.html', context)


class ProductPurchasedView(View):
    @method_decorator(login_required)
    def get(self, request):
        today = datetime.today().date()
        items_list = ProductPurchase.objects.all().order_by('created')
        projects = Projects.objects.values_list("name", flat=True)
        value_dict = {}
        for project in projects:
            total_count = 0
            purchase_orders = PurchaseOrder.objects.filter(device__name=project,delivery_status =2)
            for order in purchase_orders:
                total_count += order.ordered_count
                value_dict[order.device.name] = total_count
        print(value_dict)
        users = User.objects.filter(purchaser=True)
        bin_locations = BinLocation.objects.all()
        vendors = Vendors.objects.all()
        shelves = Shelves.objects.all()
        innerholes = Innerholes.objects.all()
        context = {
            'items_list': items_list,
            'today': today,
            'users': users,
            "bin_locations": bin_locations,
            "vendors": vendors,
            "shelves": shelves,
            "innerholes": innerholes
        }
        return render(request, 'new_product_purchased.html', context)


class PurchaseRequestView(View):
    @method_decorator(login_required)
    def get(self, request):
        items_list = PurchaseRequest.objects.all()
        overall = OverallInventory.objects.all()
        persons = User.objects.filter(purchaser=True)
        context = {
            'items_list': items_list,
            'choices': CHOICES,
            'persons': persons,
            'overall': overall,
            "projects": Projects.objects.all()
        }
        return render(request, 'new_purchase_request.html', context)


class OverallInventoryView(View):
    @method_decorator(login_required)
    def get(self, request):
        items = OverallInventory.objects.all()
        context = {
            'items_list': items,
        }
        return render(request, 'new_overall.html', context)


class OutOfStockView(View):
    @method_decorator(login_required)
    def get(self, request):
        project_name = self.request.GET.get("name")
        project = Projects.objects.get(name=project_name)
        orders = PurchaseOrder.objects.filter(device=project)
        total_count = 0
        for order in orders:
            total_count += order.ordered_count
        items_list = ProjectInventory.objects.filter(project=project)
        print(items_list)
        material_dict = {}
        for order in orders:
            out_products = ProductOut.objects.filter(project=project, purchase_order_number__order_no=order.order_no,
                                                     purchase_order_number__delivery_status=2)
            print(out_products)
            for product in out_products:
                material_dict[product.product_out.part_description] = product.out_count - product.return_count
        print(material_dict)
        data_list = []
        value = 0
        for item in items_list:
            print(item)
            part_description = item.inventory.part_description
            if part_description in material_dict.keys():
                value_dict = {}
                value = material_dict[item.inventory.part_description]
                value_dict["name"] = item.inventory.part_name
                value_dict["description"] = item.inventory.part_description
                value_dict["required"] = total_count * item.required_count
                value_dict["allocated"] = value
                if item.available:
                    value_dict["available"] = item.available.total_count
                    total_available = value_dict['allocated'] + value_dict["available"]
                    if total_count > 0 and value_dict["required"] > total_available:
                        value_dict["shortage"] = item.available.total_count + value - value_dict["required"]
                    else:
                        value_dict["shortage"] = 0
                else:
                    value_dict["available"] = 0
                    if total_count > 0:
                        value_dict["shortage"] = value - value_dict["required"]
                    else:
                        value_dict["shortage"] = 0
                value_dict["remarks"] = item.inventory.remarks
                data_list.append(value_dict)
            else:
                value = 0
                value_dict = {}
                value_dict["name"] = item.inventory.part_name
                value_dict["description"] = item.inventory.part_description
                value_dict["required"] = total_count * item.required_count
                value_dict["allocated"] = 0
                if item.available:
                    value_dict["available"] = item.available.total_count
                    total_available = value_dict['allocated'] + value_dict["available"]
                    if total_count > 0 and value_dict["required"] > total_available:
                        value_dict["shortage"] = item.available.total_count + value - value_dict["required"]
                    else:
                        value_dict["shortage"] = 0
                else:
                    value_dict["available"] = 0
                    if total_count > 0:
                        value_dict["shortage"] = value - value_dict["required"]
                    else:
                        value_dict["shortage"] = 0
                value_dict["remarks"] = item.inventory.remarks
                data_list.append(value_dict)
        print(data_list)
        context = {
            'project': project_name,
            'count': total_count,
            'items_list': data_list,
        }
        return render(request, 'out-of-stock.html', context)


class PurchaseOrderView(View):
    @method_decorator(login_required)
    def get(self, request):
        items = PurchaseOrder.objects.all()
        today = datetime.today()
        context = {
            'items_list': items,
            "today": today
        }
        return render(request, 'purchase_order.html', context)


class ProjectDetailView(View):
    @method_decorator(login_required)
    def get(self, request, **kwargs):
        project = Projects.objects.get(id=kwargs["id"])
        overall = OverallInventory.objects.all()
        try:
            items_list = ProjectInventory.objects.filter(project=project)
            context = {"items_list": items_list,
                       "project": project,
                       "overall": overall,
                       }
            return render(request, 'project_detail.html', context)
        except:
            context = {
                       "project": project,
                       "overall": overall,
            }
            return render(request, 'project_detail.html', context)


class ReturnedView(View):
    @method_decorator(login_required)
    def get(self, request):
        today = datetime.today().date()
        products = ProductOut.objects.all()
        teams = Teams.objects.all()
        projects = Projects.objects.all()
        users = User.objects.all()
        mechanical_users = users.filter(team__name="Mechanical")
        electronics_users = users.filter(team__name="Electronics")
        software_users = users.filter(team__name="Software")
        production_users = users.filter(team__name="Production")
        items_list = InventoryReturned.objects.all()
        context = {
            'items_list': items_list,
            'today': today,
            'products_out': products,
            'teams': teams,
            'projects': projects,
            "mechanical_users": mechanical_users,
            "electronics_users": electronics_users,
            "software_users": software_users,
            "production_users": production_users
        }
        return render(request, 'new_returned.html', context)

#
# class AddProductInView(View):
#
#     def post(self, request):
#         if self.request.POST['part_name']:
#             try:
#                 obj = ProductIn.objects.get(id=self.request.POST["id"])
#                 obj.part_description = self.request.POST['part_description']
#                 obj.part_name = self.request.POST['part_name']
#                 obj.save()
#             except:
#                 obj = ProductIn()
#                 obj.part_description = self.request.POST['part_description']
#                 obj.part_name = self.request.POST['part_name']
#                 items = list(ProductIn.objects.values_list('part_name', flat=True))
#                 if obj.part_name in items:
#                     data = dict()
#                     data['result'] = "Partname Already Exist"
#                     return HttpResponse(json.dumps(data), content_type="application/json")
#                 obj.save()
#             try:
#                 overall_obj = OverallInventory.objects.get(part_name=self.request.POST['old_part_name'])
#             except:
#                 items = list(OverallInventory.objects.values_list('part_name', flat=True))
#                 if request.POST['part_name'] in items:
#                     overall_obj = OverallInventory.objects.get(part_name=self.request.POST['part_name'])
#                 else:
#                     overall_obj = OverallInventory()
#             overall_obj.part_name = obj.part_name
#             overall_obj.product_in_count = obj.count
#             overall_obj.part_description = obj.part_description
#             overall_obj.product_in_count = int(obj.count)
#             overall_obj.remarks = self.request.POST.get("remarks")
#             overall_obj.total_count = overall_obj.product_in_count - overall_obj.product_out_count + \
#                                       overall_obj.product_purchased_count + overall_obj.product_returned_count
#             overall_obj.save()
#             data = dict()
#             data['result'] = "success"
#             return HttpResponse(json.dumps(data), content_type="application/json")
#         else:
#             data = dict()
#             data['result'] = "partname is mandatory"
#             return HttpResponse(json.dumps(data), content_type="application/json")


class AddProductOutView(View):

    def post(self, request):
        if request.POST['part_description'] and request.POST['count'] and request.POST["project"] and \
                            self.request.POST.get("po-number"):
            project = Projects.objects.get(id=self.request.POST.get("project"))
            purchase_order = PurchaseOrder.objects.filter(id=self.request.POST.get("po-number"), device=project)
            if len(purchase_order) >= 1:
                prod_obj = Product.objects.get(part_description=self.request.POST.get("part_description"))
                try:
                    overall_obj = OverallInventory.objects.get(product_overall=prod_obj)
                    obj = ProductOut.objects.get(id=self.request.POST["id"])
                    obj.product_out = prod_obj
                    obj.out_count = self.request.POST['count']
                    if int(overall_obj.total_count) < int(obj.out_count):
                        data = dict()
                        data['result'] = "Count you entered is greater than total count available.Available count is " + \
                                         str(overall_obj.total_count)
                        return HttpResponse(json.dumps(data),
                                            content_type="application/json")
                    today = datetime.today().strftime('%Y-%m-%d')
                    if self.request.POST['recieved_date'] > today:
                        data = dict()
                        data['result'] = "The date you selected is greater than todays date"
                        return HttpResponse(json.dumps(data), content_type="application/json")
                    else:
                        obj.recieved_date = request.POST['recieved_date']
                    project_id = request.POST.get('project')
                    team_name = request.POST.get("team")
                    purchase_order = PurchaseOrder.objects.get(id=self.request.POST.get("po-number"))
                    obj.purchase_order_number = purchase_order
                    obj.project = Projects.objects.get(id=project_id)
                    if team_name:
                        obj.team = Teams.objects.get(name=team_name)
                    person_id = request.POST.get("person")
                    if person_id:
                        obj.person = User.objects.get(id=person_id)
                    # obj.save()
                    try:
                        # total_count = 0
                        inventory_obj = ProjectInventory.objects.get(inventory=prod_obj, project=obj.project)
                        # PurchaseOrders = PurchaseOrder.objects.filter(device=project, delivery_status=2)
                        # for order in PurchaseOrders:
                        # total_count += order.oredered_count
                        if inventory_obj.required_count * purchase_order.oredered_count < int(obj.out_count):
                            data = dict()
                            data['result'] = "You are allocating more count than required.The total required count is " + \
                                             str(inventory_obj.count)
                            return HttpResponse(json.dumps(data),
                                                content_type="application/json")
                        obj.save()
                        inventory_obj.product_out = obj
                        inventory_obj.save()
                    except:
                        data = dict()
                        data['result'] = "No such item present in the BOM for this project"
                        return HttpResponse(json.dumps(data), content_type="application/json")
                except:
                    try:
                        overall_object = OverallInventory.objects.get(product_overall=prod_obj)
                        total_count = overall_object.total_count
                        if int(request.POST['count']) > total_count:
                            data = dict()
                            data['result'] = "Count you entered is greater than total count available"
                            return HttpResponse(json.dumps(data), content_type="application/json")
                    except:
                        data = dict()
                        data['result'] = "There is no such inventory available with you"
                        return HttpResponse(json.dumps(data), content_type="application/json")
                    project_id = request.POST.get('project')
                    team_name = request.POST.get("team")
                    obj = ProductOut()
                    if request.POST['recieved_date']:
                        today = datetime.today().strftime('%Y-%m-%d')
                        if request.POST['recieved_date'] > today:
                            data = dict()
                            data['result'] = "The date you selected is greater than todays date"
                            return HttpResponse(json.dumps(data), content_type="application/json")
                        else:
                            obj.recieved_date = request.POST['recieved_date']
                    obj.project = Projects.objects.get(id=project_id)
                    if team_name:
                        obj.team = Teams.objects.get(name=team_name)
                    person_id = request.POST.get("person")
                    if person_id:
                        obj.person = User.objects.get(id=person_id)
                    purchase_order_id = PurchaseOrder.objects.get(id=self.request.POST.get("po-number"))
                    obj.purchase_order_number = purchase_order_id
                    obj.out_count = request.POST['count']
                    obj.product_out = prod_obj
                    all_objects = ProductOut.objects.filter(product_out=prod_obj)
                    for item in all_objects:
                        if item.project == obj.project and str(item.recieved_date) == \
                                obj.recieved_date and item.person == obj.person:
                            data = dict()
                            data['result'] = "This same person recieved the same partname for the same project on the" \
                                             " same given date.Edit the same count"
                            return HttpResponse(json.dumps(data), content_type="application/json")
                    try:
                        inventory_obj = ProjectInventory.objects.get(inventory=prod_obj,
                                                                     project=obj.project)
                        purchase_order = PurchaseOrder.objects.get(id=self.request.POST.get("po-number"))
                        purchase_order.delivery_status = 2
                        purchase_order.save()
                        if inventory_obj.required_count * purchase_order.ordered_count < int(obj.out_count):
                            data = dict()
                            data['result'] = "You are allocating more count than required"
                            return HttpResponse(json.dumps(data),
                                                content_type="application/json")
                        obj.save()
                        inventory_obj.product_out = obj
                        inventory_obj.save()
                    except:
                        data = dict()
                        data['result'] = "There is no such item required for this project in BOM file"
                        return HttpResponse(json.dumps(data),
                                            content_type="application/json")
                try:
                    overall_obj = OverallInventory.objects.get(product_overall=prod_obj)
                    out_products = ProductOut.objects.filter(product_out=prod_obj)
                    count = 0
                    for obj in out_products:
                        count += obj.out_count
                    # overall_obj.part_description = obj.part_description
                    overall_obj.product_out_count = int(count)
                    overall_obj.total_count = overall_obj.product_purchased_count - overall_obj.product_out_count + \
                                              overall_obj.product_returned_count
                    overall_obj.save()
                    data = dict()
                    data['result'] = "success"
                    return HttpResponse(json.dumps(data),
                                        content_type="application/json")
                except:
                    data = dict()
                    data['result'] = "something went wrong"
                    return HttpResponse(json.dumps(data),
                                        content_type="application/json")
            else:
                data = dict()
                data['result'] = "There is no such purhchase order for this device"
                return HttpResponse(json.dumps(data),
                                    content_type="application/json")
        else:
            data = dict()
            data['result'] = "partname,count,project is mandatory"
            return HttpResponse(json.dumps(data),
                                content_type="application/json")


class AddProductPurchasedView(View):

    def post(self, request):
        # old_part_name = request.POST.get("old_part_name")
        # old_part_description = request.POST.get("old_part_description")

        part_description = request.POST.get("part_description")
        if request.POST['count'] and request.POST["hole"]:
            try:
                obj = ProductPurchase.objects.get(id=request.POST.get("id"))
                if request.POST.get('count'):
                    obj.purchase_count = request.POST['count']
                vendor_id = request.POST['vendor']
                vendor = Vendors.objects.get(id=vendor_id)
                obj.vendor_name = vendor
                hole_id = request.POST.get("hole")
                hole = Innerholes.objects.get(id=hole_id)
                obj.location = hole
                if request.POST['purchased_on']:
                    obj.purchased_on = request.POST['purchased_on']
                purchaser_id = request.POST.get('purchaser')
                purchaser = User.objects.get(id=purchaser_id)
                obj.purchaser = purchaser
                obj.save()
            except:
                obj = ProductPurchase()
                obj.part_description = request.POST['part_description']
                product_obj = Product.objects.get(part_description=part_description)
                obj.product_purchase = product_obj
                obj.purchase_count = request.POST['count']
                purchaser_id = request.POST.get('purchaser')
                purchaser = User.objects.get(id=purchaser_id)
                obj.purchaser = purchaser
                vendor_id = request.POST.get('vendor')
                vendor = Vendors.objects.get(id=vendor_id)
                obj.vendor_name = vendor
                hole = Innerholes.objects.get(id=request.POST["hole"])
                obj.location = hole
                if request.POST['purchased_on']:
                    today = datetime.today().strftime('%Y-%m-%d')
                    if request.POST['purchased_on'] > today:
                        data = dict()
                        data['result'] = "The date you selected is greater than todays date"
                        return HttpResponse(json.dumps(data), content_type="application/json")
                    else:
                        obj.purchased_on = request.POST['purchased_on']
                obj.save()
            try:
                overall_obj = OverallInventory.objects.get(product_overall__part_description=part_description)
            except:
                # items = list(OverallInventory.objects.values_list('part_description', flat=True))
                # if part_description in items:
                #     overall_obj = OverallInventory.objects.get(part_description=part_description)
                # else:
                overall_obj = OverallInventory()
            purchased = ProductPurchase.objects.filter(product_purchase__part_description=part_description)
            count = 0
            for obj in purchased:
                count += obj.purchase_count
            overall_obj.product_overall = Product.objects.get(part_description=part_description)
            overall_obj.product_purchased_count = int(count)
            overall_obj.location = hole
            overall_obj.total_count = overall_obj.product_purchased_count - overall_obj.product_out_count + \
                                       overall_obj.product_returned_count
            overall_obj.save()
            project_inventories = ProjectInventory.objects.filter(inventory__part_description=part_description)
            for inventory_obj in project_inventories:
                inventory_obj.available = overall_obj
                inventory_obj.save()
            # if not old_part_description == part_description:
            #     try:
            #         out_objs = ProductOut.objects.filter(Q(part_name=request.POST.get("old_part_name"))
            #                                              | Q(part_description=request.POST.get
            #                                              ("old_part_description")))
            #         for obj in out_objs:
            #             obj.part_name = request.POST.get("part_name")
            #             obj.part_description = request.POST.get("part_description")
            #             obj.save()
            #     except:
            #         pass
            #     try:
            #         return_objs = InventoryReturned.objects.filter(Q(part_name=request.POST.get("old_part_name")) |
            #                                                        Q(part_description=request.POST.get
            #                                                        ("old_part_description")))
            #         for obj in return_objs:
            #             obj.part_name = request.POST.get("part_name")
            #             obj.part_description = request.POST.get("part_description")
            #             obj.save()
            #     except:
            #         pass
            data = dict()
            data['result'] = "success"
            return HttpResponse(json.dumps(data),
                                content_type="application/json")
        else:
            data = dict()
            data['result'] = "part_description,count,shelf,box and hole is mandatory"
            return HttpResponse(json.dumps(data),
                                content_type="application/json")


class AddInventoryReturnView(View):
    def post(self, request):
        if request.POST.get('part_description') and request.POST.get('count') and request.POST.get("project"):
            prod_obj = Product.objects.get(part_description=request.POST.get("part_description"))
            try:
                obj = InventoryReturned.objects.get(id=request.POST["id"])
                obj.product_returned = prod_obj
                # team_name = request.POST["team"]
                project_name = request.POST["project"]
                # person_id = request.POST.get("person")
                # user = User.objects.get(id=person_id)
                if request.POST.get('count'):
                    obj.count_returned = request.POST['count']
                try:
                    out_obj = ProductOut.objects.get(product_out=prod_obj,
                                                     project__name=project_name)
                    if out_obj.out_count < int(obj.count_returned):
                        data = dict()
                        data['result'] = "The count you are returning is greater than count you recieved"
                        return HttpResponse(json.dumps(data), content_type="application/json")
                except:
                    data = dict()
                    data['result'] = "There is no such item you allocated"
                    return HttpResponse(json.dumps(data), content_type="application/json")
                if request.POST['returned_date']:
                    today = datetime.today().strftime('%Y-%m-%d')
                    date = out_obj.recieved_date.strftime('%Y-%m-%d')
                    if request.POST['returned_date'] > today:
                        data = dict()
                        data['result'] = "The date you selected is greater than todays date"
                        return HttpResponse(json.dumps(data),
                                            content_type="application/json")
                    if request.POST['returned_date'] < date:
                        data = dict()
                        data['result'] = "The date you selected is lesser than date you transferred this inventory"
                        return HttpResponse(json.dumps(data),
                                            content_type="application/json")
                    else:
                        obj.returned_date = request.POST['returned_date']
                obj.out_object = out_obj
                out_obj.return_count = obj.count_returned
                out_obj.save()
                obj.save()
            except:
                obj = InventoryReturned()
                # items = ProductOut.objects.all()
                # team_name = request.POST.get("team")
                project_name = request.POST.get("project")
                person_id = request.POST.get("person")
                # team = Teams.objects.get(name=team_name)
                project = Projects.objects.get(name=project_name)
                # person = User.objects.get(id=person_id)
                # part_names = items.values_list("part_name", flat=True)
                # projects = items.values_list("project__name", flat=True)
                # teams = items.values_list("team__name", flat=True)
                # persons = items.values_list("person__first_name", flat=True)
                obj.part_description = request.POST.get('part_description')
                obj.part_name = request.POST.get('part_name')
                if request.POST.get('count'):
                    obj.count_returned = request.POST['count']
                obj.project = project
                # obj.team = team
                # obj.person = person
                try:
                    out_obj = ProductOut.objects.get(product_out=prod_obj,
                                                     project=obj.project)
                    if out_obj.out_count < int(obj.count_returned):
                        data = dict()
                        data['result'] = "The count you are returning is greater than count you recieved"
                        return HttpResponse(json.dumps(data), content_type="application/json")
                except:
                    data = dict()
                    data['result'] = "There is no such item you allocated"
                    return HttpResponse(json.dumps(data), content_type="application/json")
                if request.POST['returned_date']:
                    today = datetime.today().strftime('%Y-%m-%d')
                    date = out_obj.recieved_date.strftime('%Y-%m-%d')
                    if request.POST['returned_date'] > today:
                        data = dict()
                        data['result'] = "The date you selected is greater than todays date"
                        return HttpResponse(json.dumps(data), content_type="application/json")
                    if request.POST['returned_date'] < date:
                        data = dict()
                        data['result'] = "The date you selected is lesser than date you transferred this inventory"
                        return HttpResponse(json.dumps(data), content_type="application/json")
                    obj.returned_date = request.POST['returned_date']
                # if not obj.part_name in part_names or not obj.project.name in projects or not obj.team.name in teams\
                #         or obj.person in persons:
                #     data = dict()
                #     data['result'] = "There is no such item out from you for this project to this person." \
                #                      "Check allocated items"
                #     return HttpResponse(json.dumps(data), content_type="application/json")
                returned_items = InventoryReturned.objects.all()
                for item in returned_items:
                    if obj.product_returned == item.product_returned and project_name == item.project.name and \
                            obj.returned_date == str(item.returned_date):
                        data = dict()
                        data['result'] = "This same person returned same item from same project on same day." \
                                         "Add the count with same item"
                        return HttpResponse(json.dumps(data), content_type="application/json")
                obj.out_object = out_obj
                obj.product_returned = prod_obj
                out_obj.return_count = obj.count_returned
                out_obj.save()
                obj.save()
            try:
                overall_obj = OverallInventory.objects.get(product_overall=prod_obj)
            except:
                overall_obj = OverallInventory()
            returned = InventoryReturned.objects.filter(product_returned=prod_obj)
            count = 0
            for obj in returned:
                count += obj.count_returned
            overall_obj.product_returned_count = int(count)
            overall_obj.total_count = overall_obj.product_purchased_count - overall_obj.product_out_count + \
                                      overall_obj.product_returned_count
            overall_obj.save()
            data = dict()
            data['result'] = "success"
            return HttpResponse(json.dumps(data),
                                content_type="application/json")
        else:
            data = dict()
            data['result'] = "part description,count,project and team is mandatory"
            return HttpResponse(json.dumps(data),
                                content_type="application/json")


class AddPurchaseRequestView(View):

    def post(self, request):
        obj = PurchaseRequest()
        if request.POST["part_description"] and request.POST["email"] and request.POST['count'] and \
                request.POST["project"]:
            obj.product_purchase = Product.objects.get(part_description=request.POST["part_description"])
            email = validate_mail(request.POST["email"])
            if email:
                obj.requested_by = request.POST["email"]
            else:
                data = dict()
                data['result'] = "Please enter a valid email address"
                return HttpResponse(json.dumps(data), content_type="application/json")
            obj.count = request.POST["count"]
            project = request.POST["project"]
            project_obj = Projects.objects.get(id=project)
            obj.project = project_obj
            if not request.POST.get("purchaser"):
                data = dict()
                data['result'] = "You must select a purchaser to continue.Contact administrator if no purchasers " \
                                 "available"
                return HttpResponse(json.dumps(data), content_type="application/json")
            try:
                purchaser = request.POST.get("purchaser")
                user_obj = User.objects.get(email=purchaser)
                obj.purchaser = user_obj
            except:
                pass
            today = datetime.today().strftime('%Y-%m-%d')
            obj.requested_on = today
            obj.status = "1"
            obj.save()
            data = dict()
            data['result'] = "success"
            return HttpResponse(json.dumps(data), content_type="application/json")
        else:
            data = dict()
            data['result'] = "part_description,email and count is mandatory"
            return HttpResponse(json.dumps(data), content_type="application/json")


class AddBomView(View):

    @method_decorator(login_required)
    def post(self, request):
        part_description = request.POST.get("part_description")
        count = request.POST.get("count")
        if part_description:
            try:
                id = request.POST.get("id")
                obj = ProjectInventory.objects.get(id=id)
                obj.required_count = count
                obj.save()
                data = dict()
                data['result'] = "success"
                return HttpResponse(json.dumps(data), content_type="application/json")
            except:
                obj = ProjectInventory()
                product_obj = Product.objects.get(part_description=part_description)
                obj.inventory = product_obj
                obj.required_count = count
                try:
                    overall_obj = OverallInventory.objects.get(product_overall__part_description=part_description)
                    obj.available = overall_obj
                except:
                    pass
                obj.save()
                project = Projects.objects.get(name=request.POST.get("project"))
                obj.project.add(project)
                data = dict()
                data['result'] = "success"
                return HttpResponse(json.dumps(data), content_type="application/json")
        else:
            data = dict()
            data['result'] = "part_description is mandatory"
            return HttpResponse(json.dumps(data), content_type="application/json")




# class DeleteProductInView(View):
#     @method_decorator(login_required)
#     def get(self, form):
#         id = self.request.GET.get("id")
#         obj = ProductIn.objects.get(id=id)
#         count = int(obj.count)
#         part_name = obj.part_name
#         try:
#             ovr_obj = OverallInventory.objects.get(part_name=part_name)
#             if ovr_obj.product_in_count > 0:
#                 ovr_obj.product_in_count = ovr_obj.product_in_count - count
#             ovr_obj.remarks = ''
#             ovr_obj.bin_location = ''
#             ovr_obj.total_count = ovr_obj.product_in_count - ovr_obj.product_out_count + \
#                                   ovr_obj.product_purchased_count + ovr_obj.product_returned_count
#             ovr_obj.save()
#         except:
#             pass
#         obj.delete()
#         return JsonResponse(
#             {"success": "Product Deleted Successfully"})


class DeleteProductOutView(View):
    @method_decorator(login_required)
    def get(self, request):
        id = request.GET.get("id")
        obj = ProductOut.objects.get(id=id)
        count = int(obj.out_count)
        returned = obj.out_inventory.all()
        count_returned = None
        for item in returned:
            count_returned = item.count_returned
        prod_obj = obj.product_out
        try:
            ovr_obj = OverallInventory.objects.get(product_overall=prod_obj)
            if ovr_obj.product_returned_count <= 0:
                ovr_obj.product_out_count = ovr_obj.product_out_count - count
                ovr_obj.total_count = ovr_obj.total_count + count
            elif ovr_obj.product_returned_count > 0:
                ovr_obj.product_out_count = ovr_obj.product_out_count - count
                ovr_obj.product_returned_count = ovr_obj.product_returned_count - count_returned
            ovr_obj.total_count = ovr_obj.product_purchased_count - ovr_obj.product_out_count +\
                                  ovr_obj.product_returned_count
            if ovr_obj.total_count < 0:
                ovr_obj.total_count = ovr_obj.product_purchased_count
            ovr_obj.save()
        except:
            pass
        obj.delete()
        return JsonResponse({"success": "Product Deleted Successfully"})


class DeleteProductPurchasedView(View):
    @method_decorator(login_required)
    def get(self, request):
        id = request.GET.get("id")
        obj = ProductPurchase.objects.get(id=id)
        count = int(obj.purchase_count)
        part_description = obj.product_purchase.part_name
        try:
            ovr_obj = OverallInventory.objects.get(product_overall__part_description=part_description)
            if ovr_obj.product_purchased_count > 0:
                ovr_obj.product_purchased_count = ovr_obj.product_purchased_count - count
            ovr_obj.total_count = ovr_obj.product_purchased_count - ovr_obj.product_out_count + \
                                  ovr_obj.product_returned_count
            ovr_obj.save()
        except:
            pass
        obj.delete()
        return JsonResponse(
            {"success": "Product Deleted Successfully"})


class DeleteProductView(View):
    @method_decorator(login_required)
    def get(self, request):
        id = request.GET.get("id")
        obj = Product.objects.get(id=id)
        obj.delete()
        return JsonResponse(
            {"success": "Product Deleted Successfully"})


class DeleteBoxView(View):
    @method_decorator(login_required)
    def get(self, request):
        id = request.GET.get("id")
        obj = BinLocation.objects.get(id=id)
        obj.delete()
        return JsonResponse(
            {"success": "Product Deleted Successfully"})


class DeleteHoleView(View):
    @method_decorator(login_required)
    def get(self, request):
        id = request.GET.get("id")
        obj = Innerholes.objects.get(id=id)
        obj.delete()
        return JsonResponse(
            {"success": "Product Deleted Successfully"})


class DeleteProjectView(View):
    @method_decorator(login_required)
    def get(self, request):
        id = request.GET.get("id")
        obj = Projects.objects.get(id=id)
        obj.delete()
        return JsonResponse(
            {"success": "Product Deleted Successfully"})


class DeleteModelView(View):
    @method_decorator(login_required)
    def get(self, request):
        id = request.GET.get("id")
        obj = ModelNames.objects.get(id=id)
        obj.delete()
        return JsonResponse(
            {"success": "Product Deleted Successfully"})


class DeleteAccessoryView(View):
    @method_decorator(login_required)
    def get(self, request):
        id = request.GET.get("id")
        obj = Accessories.objects.get(id=id)
        obj.delete()
        return JsonResponse(
            {"success": "Product Deleted Successfully"})


class DeleteShelfView(View):
    @method_decorator(login_required)
    def get(self, request):
        id = request.GET.get("id")
        obj = Shelves.objects.get(id=id)
        obj.delete()
        return JsonResponse(
            {"success": "Product Deleted Successfully"})


class DeleteInventoryReturnView(View):
    @method_decorator(login_required)
    def get(self, request):
        id = request.GET.get("id")
        obj = InventoryReturned.objects.get(id=id)
        count = int(obj.count_returned)
        prod_obj = obj.product_returned
        try:
            ovr_obj = OverallInventory.objects.get(product_overall=prod_obj)
            if ovr_obj.product_returned_count > 0:
                ovr_obj.product_returned_count = ovr_obj.product_returned_count - count
            ovr_obj.total_count = ovr_obj.product_purchased_count - ovr_obj.product_in_count - ovr_obj.product_out_count +  + ovr_obj.product_returned_count
            ovr_obj.save()
        except:
            pass
        obj.delete()
        return JsonResponse(
            {"success": "Product Deleted Successfully"})


class DeleteFromBomListView(View):
    @method_decorator(login_required)
    def get(self, request):
        id = request.GET.get("id")
        obj = ProjectInventory.objects.get(id=id)
        obj.delete()
        return JsonResponse(
            {"success": "Product Deleted Successfully"})


def export_xls_in(request):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="inventory.xls"'
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Users')
    # Sheet header, first row
    row_num = 0
    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    # font_style.num_format_str = 'dd/mm/yyyy'
    columns = ['Part Name', 'Part Description', 'Remarks', ]
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()
    # font_style.num_format_str = 'dd/mm/yyyy'
    rows = Product.objects.all().values_list('part_name', 'part_description', 'remarks',)
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)
    wb.save(response)
    return response


def export_xls_out(request):
    response = HttpResponse(content_type='application/ms-excel')
    response[
        'Content-Disposition'] = 'attachment; filename="inventory_out.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Users')
    # Sheet header, first row
    row_num = 0
    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    # font_style.num_format_str = 'dd/mm/yyyy'
    columns = ['Part Description', 'Count', 'recieved_by', 'recieved_date', 'project', ]
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()
    # font_style.num_format_str = 'dd/mm/yyyy'
    rows = ProductOut.objects.all().values_list('product_out__part_description', 'out_count', "recieved_by",
                                                 'recieved_date', 'project',)
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)
    wb.save(response)
    return response


def export_xls_purchased(request):
    response = HttpResponse(content_type='application/ms-excel')
    response[
        'Content-Disposition'] = 'attachment; filename="inventory_purchased.xls"'
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Users')
    # Sheet header, first row
    row_num = 0
    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    # font_style.num_format_str = 'dd/mm/yyyy'
    columns = ['Part Description', 'Count', 'vendor',
               'purchaser', 'purchased_on', ]
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()
    # font_style.num_format_str = 'dd/mm/yyyy'
    rows = ProductPurchase.objects.all().values_list('product_purchase__part_description', 'purchase_count', 'vendor_name__name',
                                                     "purchaser__first_name", 'purchased_on',)
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)
    wb.save(response)
    return response


def export_xls_returned(request):
    response = HttpResponse(content_type='application/ms-excel')
    response[
        'Content-Disposition'] = 'attachment; filename="inventory_returned.xls"'
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Users')
    # Sheet header, first row
    row_num = 0
    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    # font_style.num_format_str = 'dd/mm/yyyy'
    columns = ['part_description', 'count_returned', 'returned_by', 'returned_date', 'project', ]
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()
    font_style.num_format_str = 'dd/mm/yyyy'
    rows = InventoryReturned.objects.all().values_list('product_returned__part_description', 'count_returned',
                                                       'returned_by', 'returned_date', 'project',)
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)
    wb.save(response)
    return response


def export_xls_overall(request):
    response = HttpResponse(content_type='application/ms-excel')
    response[
        'Content-Disposition'] = 'attachment; filename="inventory_overall.xls"'
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Users')
    # Sheet header, first row
    row_num = 0
    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    # font_style.num_format_str = 'dd/mm/yyyy'
    columns = ['part_name', 'part_description', 'total_count',
               'shelf', 'box', 'hole', 'remarks', ]
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()
    # font_style.num_format_str = 'dd/mm/yyyy'
    rows = OverallInventory.objects.all().values_list('product_overall__part_name', 'product_overall__part_description',
                                                      'total_count', 'location__shelf__name', 'location__bin_obj__name',
                                                      'location__name', 'product_overall__remarks',)
    print(rows)
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response


def export_xls_purchase_request(request):
    response = HttpResponse(content_type='application/ms-excel')
    response[
        'Content-Disposition'] = 'attachment; filename="purchase_request.xls"'
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Users')
    # Sheet header, first row
    row_num = 0
    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    # font_style.num_format_str = 'dd/mm/yyyy'
    columns = ['part_description', 'request-no', 'count'
               'requested_by', 'requested_on', 'status']
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()
    font_style.num_format_str = 'dd/mm/yyyy'
    rows = PurchaseRequest.objects.all().values_list('product_purchase__part_description', 'request_number', 'count',
                                                     'requested_by', 'requested_on', 'status',)
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)
    wb.save(response)
    return response


def export_xls_bom_list(request):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename=' + request.GET.get("project") + 'BoM.xls'
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Users')
    # Sheet header, first row
    row_num = 0
    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    # font_style.num_format_str = 'dd/mm/yyyy'
    columns = ['part_description', 'required-count', 'available-count']
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()
    # font_style.num_format_str = 'dd/mm/yyyy'
    rows = ProjectInventory.objects.filter(project__name=request.GET.get("project")).values_list(
         'inventory__part_description', 'required_count', 'available__total_count',)
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)
    wb.save(response)
    return response


class ProductView(View):
    @method_decorator(login_required)
    def get(self, request):
        items = Product.objects.all()
        context = {
            'items_list': items,
        }
        return render(request, 'product.html', context)


class ProjectView(View):
    @method_decorator(login_required)
    def get(self, request):
        items = Projects.objects.all()
        context = {
            'items_list': items,
        }
        return render(request, 'projects.html', context)


class ModelView(View):
    @method_decorator(login_required)
    def get(self, request):
        items = ModelNames.objects.all()
        accessories = Accessories.objects.all()
        today = datetime.today()
        print(items)
        context = {
            'items_list': items,
            'accessories': accessories,
            'today': today
        }
        return render(request, 'models.html', context)


class AccessoryView(View):
    @method_decorator(login_required)
    def get(self, request):
        accessories = Accessories.objects.all()
        today = datetime.today()
        context = {
            'accessories': accessories,
            'today': today,
            # 'models': models,
        }
        return render(request, 'accessories.html', context)


class ShelvesView(View):
    @method_decorator(login_required)
    def get(self, request):
        items = Shelves.objects.all()
        context = {
            'items_list': items,
        }
        return render(request, 'shelves.html', context)


class BoxesView(View):
    @method_decorator(login_required)
    def get(self, request):
        items = BinLocation.objects.all()
        context = {
            'items_list': items,
        }
        return render(request, 'boxes.html', context)


class HolesView(View):
    @method_decorator(login_required)
    def get(self, request):
        items = Innerholes.objects.all()
        bins = BinLocation.objects.all()
        shelves = Shelves.objects.all()
        context = {
            'items_list': items,
            'bins': bins,
            'shelves': shelves
        }
        return render(request, 'holes.html', context)


class AddBoxView(View):
    def post(self, request):
        try:
            bin_obj = BinLocation.objects.get(id=request.POST.get("id"))
        except:
            bin_obj = BinLocation()
        name = request.POST.get('box_name')
        if not name:
            data = dict()
            data['result'] = "Please Fill The Field"
            return HttpResponse(json.dumps(data),
                                content_type="application/json")
        bin_obj.name = name
        bin_obj.save()
        data = dict()
        data['result'] = "success"
        return HttpResponse(json.dumps(data), content_type="application/json")


class AddDeviceOrderView(View):
    def post(self, request):
        print(self.request.POST)
        try:
            order_obj = PurchaseOrder.objects.get(id=request.POST.get("id"))
        except:
            order_obj = PurchaseOrder()
        order_obj.order_no = self.request.POST.get("order_number")
        order_obj.destination = self.request.POST.get("destination")
        order_obj.ordered_count = int(self.request.POST.get("count"))
        device = Projects.objects.get(id=request.POST.get("project"))
        order_obj.device = device
        order_obj.placed_on = request.POST.get("placed_on")
        order_obj.total_delivered = 0
        order_obj.save()
        data = dict()
        data['result'] = "success"
        return HttpResponse(json.dumps(data), content_type="application/json")


class NewMachineView(View):
    def post(self, request):
        print(self.request.POST)
        if self.request.POST.get("po-number") and self.request.POST.get("default-plate") and \
                self.request.POST.get("serial"):
            try:
                machine_obj = MachineSerialNumber.objects.get(id=request.POST.get("id"))
                machines = MachineSerialNumber.objects.all().exclude(id=machine_obj.id)
            except:
                machine_obj = MachineSerialNumber()
                machines = MachineSerialNumber.objects.all()
            po_number = PurchaseOrder.objects.get(id=self.request.POST.get("po-number"))
            for machine in machines:
                if machine.serial_no == int(self.request.POST.get("serial")) and \
                        machine.po_number.order_no == po_number.order_no:
                    data = dict()
                    data['result'] = "You already have a device with this serial number for this particular order"
                    return HttpResponse(json.dumps(data), content_type="application/json")
            machine_obj.serial_no = self.request.POST.get("serial")

            if int(machine_obj.serial_no) > po_number.ordered_count:
                data = dict()
                data['result'] = "The serial Number You provides exceeds the count ordered by the customer"
                return HttpResponse(json.dumps(data), content_type="application/json")
            machine_obj.po_number = po_number
            default_plate = TopPlates.objects.get(id=self.request.POST.get("default-plate"))
            if self.request.POST.get("extra-plate"):
                extra_plate = TopPlates.objects.get(id=self.request.POST.get("extra-plate"))
                machine_obj.extra_top_plate = extra_plate
            machine_obj.default_top_plate = default_plate
            if self.request.POST.get("week"):
                work_week = WorkWeek.objects.get(id=self.request.POST.get("week"))
                machine_obj.delivery_work_week = work_week
            machine_obj.delivery_status = self.request.POST.get("status")
            machine_obj.invoiced = self.request.POST.get("bool")
            machine_obj.save()
            machines = MachineSerialNumber.objects.filter(po_number=po_number, delivery_status=3)
            po_number.total_delivered = len(machines)
            po_number.save()
            data = dict()
            data['result'] = "success"
            return HttpResponse(json.dumps(data), content_type="application/json")
        else:
            data = dict()
            data['result'] = "Serial Number, PO number and Top-plate is mandatory"
            return HttpResponse(json.dumps(data), content_type="application/json")

class DeviceTrackerView(View):
    @method_decorator(login_required)
    def get(self, request):
        items = MachineSerialNumber.objects.all().order_by("serial_no")
        purchase_orders = PurchaseOrder.objects.all()
        top_plates = TopPlates.objects.all()
        work_weeks = WorkWeek.objects.all()
        choices = CHOICES_3
        choices_1 =  CHOICES_2

        context = {
            'items_list': items,
            'orders': purchase_orders,
            "plates": top_plates,
            "choices": choices,
            "choices_1": choices_1,
            "weeks": work_weeks
        }
        return render(request, 'device_tracker.html', context)


class AddHoleView(View):
    def post(self, request):
        try:
            hole = Innerholes.objects.get(id=request.POST.get("id"))
        except:
            hole = Innerholes()
        name = request.POST.get('hole-name')
        box = BinLocation.objects.get(id=request.POST.get("box-name"))
        shelf = Shelves.objects.get(id=request.POST.get("shelf-name"))
        if not name or not request.POST["shelf-name"] or not request.POST['box-name']:
            data = dict()
            data['result'] = "Please Fill The Fields"
            return HttpResponse(json.dumps(data),
                                content_type="application/json")
        hole.name = name
        hole.bin_obj = box
        hole.shelf = shelf
        hole.save()
        data = dict()
        data['result'] = "success"
        return HttpResponse(json.dumps(data), content_type="application/json")


class AddProductView(View):
    def post(self, request):
        print(request.POST)
        try:
            prod_obj = Product.objects.get(id=request.POST.get("id"))
        except:
            prod_obj = Product()
        part_name = request.POST.get('part_name')
        print(part_name)
        part_description = request.POST.get("part_description")
        print(part_description)
        remarks = request.POST.get("remarks")
        if part_name and part_description:
            prod_obj.part_name = part_name
            prod_obj.part_description = part_description
            prod_obj.remarks = remarks
            prod_obj.save()
            data = dict()
            data['result'] = "success"
            return HttpResponse(json.dumps(data), content_type="application/json")
        else:
            data = dict()
            data['result'] = "Part name and Part description are mandatory"
            return HttpResponse(json.dumps(data),
                                content_type="application/json")


class AddProjectView(View):
    def post(self, request):
        try:
            proj_obj = Projects.objects.get(id=request.POST.get("id"))
        except:
            proj_obj = Projects()
        name = request.POST.get('project_name')
        if not name:
            data = dict()
            data['result'] = "Please Fill The Field"
            return HttpResponse(json.dumps(data),
                                content_type="application/json")
        proj_obj.name = name
        proj_obj.save()
        data = dict()
        data['result'] = "success"
        return HttpResponse(json.dumps(data), content_type="application/json")


class AddModelView(View):
    def post(self, request):
        if request.POST.get("model_name") and request.POST.get("count_delivered"):
            try:
                model_obj = ModelNames.objects.get(id=request.POST.get("id"))
            except:
                model_obj = ModelNames()
            model_obj.model_name = request.POST.get('model_name')
            model_obj.count_delivered = request.POST.get('count_delivered')
            model_obj.date_of_manufacture = request.POST.get('date_of_manufacture')
            model_obj.remarks = request.POST.get('remarks')
            model_obj.item_code = request.POST.get('item_code')
            model_obj.voltage = request.POST.get("voltage")
            model_obj.save()
            data = dict()
            data['result'] = "success"
            return HttpResponse(json.dumps(data), content_type="application/json")
        else:
            data = dict()
            data['result'] = "Model name and count delivered is Mandatory"
            return HttpResponse(json.dumps(data), content_type="application/json")


class AddAccessoryView(View):
    def post(self, request):
        if request.POST.get("name"):
            try:
                obj = Accessories.objects.get(id=request.POST.get("id"))
            except:
                obj = Accessories()
            obj.name = request.POST.get('name')
            obj.item_code = request.POST.get('item_code')
            # models = request.POST.getlist('models')
            # try:
            #     current_models = obj.model_names.all()
            #     for model in current_models:
            #         obj.model_names.remove(model)
            # except:
            #     pass
            # for model in models:
            #     obj.model_names.add(model)
            obj.save()
            data = dict()
            data['result'] = "success"
            return HttpResponse(json.dumps(data), content_type="application/json")
        else:
            data = dict()
            data['result'] = "name is Mandatory"
            return HttpResponse(json.dumps(data), content_type="application/json")


class AddShelfView(View):
    def post(self, request):
        try:
            proj_obj = Shelves.objects.get(id=request.POST.get("id"))
        except:
            proj_obj = Shelves()
        name = request.POST.get('shelf_name')
        if not name:
            data = dict()
            data['result'] = "Please Fill The Field"
            return HttpResponse(json.dumps(data),
                                content_type="application/json")
        proj_obj.name = name
        proj_obj.save()
        data = dict()
        data['result'] = "success"
        return HttpResponse(json.dumps(data), content_type="application/json")


class ChangeStatusView(View):
    def post(self, request):
        print(request.POST)
        obj = PurchaseRequest.objects.get(id=request.POST['id'])
        obj.status = request.POST.get('status')
        obj.save()
        message = render_to_string('request_confirmation_mail.html', {
            'part_name': obj.product_purchase.part_name,
            'part_description': obj.product_purchase.part_description,
            'count': obj.count,
            'request_no': obj.request_number,
            "requested_on": obj.requested_on,
            "status": obj.get_status_display(),
            "project": obj.project,
            "requested_by": obj.requested_by,
        })
        to_email = obj.requested_by
        send_mail(
            'Status Changed.',
            'RandD Inventory',
            '147444@ust-global.com',
            [to_email],
            html_message=message, )

        data = dict()
        data['result'] = "success"
        return HttpResponse(json.dumps(data), content_type="application/json")


class ModelDetailView(View):
    @method_decorator(login_required)
    def get(self, request, **kwargs):
        obj = ModelNames.objects.get(id=kwargs["id"])
        print(obj)
        accessories = Accessories.objects.filter(model_names=obj)
        print(accessories)
        table_headings = []
        for i in range(1, obj.count_delivered+1):
            table_headings.append(obj.item_code+str(i).zfill(4))
        print(table_headings)
        context = {
            'accessories': accessories,
            "model": obj,
            "headings": table_headings
        }
        return render(request, 'model-detail.html', context)


class AccessoryDetailView(View):
    @method_decorator(login_required)
    def get(self, request,**kwargs):
        total_delivered = 0
        obj = Accessories.objects.get(id=kwargs["id"])
        accessories_delivered = AccessoryDelivered.objects.filter(accessory=obj)
        machines = MachineSerialNumber.objects.filter(delivery_status=3)
        orders = PurchaseOrder.objects.all()
        # models = ModelNames.objects.all()
        # print(accessories)
        datalist = []
        for order in orders:
            values_dict = {}
            machines_associated = list(machines.filter(po_number=order))
            values_dict[order.order_no] = machines_associated
            datalist.append(values_dict)
        print(datalist)
        for item in accessories_delivered:
            total_delivered += item.count_delivered
        print(obj)
        context = {
            "accessories": accessories_delivered,
            "total": total_delivered,
            "object": obj,
            "list": datalist,
            'orders': orders
        }
        return render(request, 'accessory-detail.html', context)
