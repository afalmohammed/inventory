from .models import Product, Projects, ModelNames, Accessories, OverallInventory


def projects(request):
    context = {
        "projects": Projects.objects.all().order_by('created'),
        "products": Product.objects.all(),
        "models": ModelNames.objects.all().order_by('created'),
        "all_accessories": Accessories.objects.all().order_by('created'),
        "item_descriptions": OverallInventory.objects.all().values("product_overall__part_description")
    }
    return context
