from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from .views import export_xls_in, ProductOutView, \
    AddProductOutView, ProductPurchasedView, OverallInventoryView, AddProductPurchasedView, DeleteProductOutView, \
    DeleteProductPurchasedView, DeleteBoxView, ReturnedView, AddInventoryReturnView, \
    DeleteInventoryReturnView, export_xls_out, export_xls_purchased, export_xls_returned, export_xls_overall, \
    PurchaseRequestView, export_xls_purchase_request, AddPurchaseRequestView, ChangeStatusView, \
    ProjectDetailView, AddBomView, DeleteFromBomListView, export_xls_bom_list, ProjectView, AddProjectView, \
    DeleteProjectView, ShelvesView, AddShelfView, DeleteShelfView, BoxesView, AddBoxView, HolesView, \
    AddHoleView, DeleteHoleView, ModelDetailView, ModelView, AddModelView, DeleteModelView, AccessoryView, \
    AccessoryDetailView, AddAccessoryView, DeleteAccessoryView, AddProductView, ProductView, DeleteProductView, \
    PurchaseOrderView, AddDeviceOrderView, DeviceTrackerView, NewMachineView, OutOfStockView

urlpatterns = [
    url(r'^product-out/$', ProductOutView.as_view(), name="product-out"),
    url(r'^product-purchased/$', ProductPurchasedView.as_view(), name="product-purchased"),
    url(r'^purchase-request/$', PurchaseRequestView.as_view(), name="purchase-request"),
    url(r'^products/$', ProductView.as_view(), name="products"),
    url(r'^projects/$', ProjectView.as_view(), name="projects"),
    url(r'^models/$', ModelView.as_view(), name="models"),
    url(r'^accessories/$', AccessoryView.as_view(), name="accessories"),
    url(r'^shelves/$', ShelvesView.as_view(), name="shelves"),
    url(r'^boxes/$', BoxesView.as_view(), name="boxes"),
    url(r'^holes/$', HolesView.as_view(), name="holes"),
    url(r'^purchase-orders/$', PurchaseOrderView.as_view(), name="orders"),
    url(r'^device-tracker/$', DeviceTrackerView.as_view(), name="tracker"),
    url(r'^out-of-stock/$', OutOfStockView.as_view(), name="out-of-stock"),
    url(r'^inventory-returned/$', ReturnedView.as_view(), name="returned"),
    url(r'^overall-inventory/$', OverallInventoryView.as_view(), name="overall"),
    url(r'^add-inventory-return/$', AddInventoryReturnView.as_view(), name="add-inventory-return"),
    url(r'^add-box/$', AddBoxView.as_view(), name="add-box"),
    url(r'^add-hole/$', AddHoleView.as_view(), name="add-hole"),
    url(r'^add-project/$', AddProjectView.as_view(), name="add-project"),
    url(r'^add-device-order/$', AddDeviceOrderView.as_view(), name="add-device-order"),
    url(r'^add-device/$', NewMachineView.as_view(), name="add-device"),
    url(r'^add-product/$', AddProductView.as_view(), name="add-product"),
    url(r'^add-model/$', AddModelView.as_view(), name="add-model"),
    url(r'^add-accessory/$', AddAccessoryView.as_view(), name="add-accessory"),
    url(r'^add-shelf/$', AddShelfView.as_view(), name="add-shelf"),
    url(r'^add-product-out/$', AddProductOutView.as_view(), name="add-product-out"),
    url(r'^add-product-purchased/$', AddProductPurchasedView.as_view(), name="add-product-purchased"),
    url(r'^add-purchase-request/$', AddPurchaseRequestView.as_view(), name="add-purchase-request"),
    url(r'^add-bom/$', AddBomView.as_view(), name="add-bom"),
    url(r'^project/(?P<id>[0-9]+)/$', ProjectDetailView.as_view(), name="project"),
    url(r'^model-detail/(?P<id>[0-9]+)/$', ModelDetailView.as_view(), name="model-detail"),
    url(r'^accessory-detail/(?P<id>[0-9]+)/$', AccessoryDetailView.as_view(), name="accessory-detail"),
    url(r'^change-status/$', ChangeStatusView.as_view(), name="change-status"),
    url(r'^export/xls/product-in/$', export_xls_in, name='export_product_in_xls'),
    url(r'^export/xls/product-out/$', export_xls_out, name='export_product_out_xls'),
    url(r'^export/xls/product-purchased/$', export_xls_purchased, name='export_product_purchased_xls'),
    url(r'^export/xls/product-returned/$', export_xls_returned, name='export_product_returned_xls'),
    url(r'^export/xls/product-overall/$', export_xls_overall, name='export_product_overall_xls'),
    url(r'^export/xls/purchase-request/$', export_xls_purchase_request, name='export_purchase_request_xls'),
    url(r'^export/xls/bom-list/$', export_xls_bom_list, name='export_bom_list_xls'),
    # url(r'^import/xls/$', DataImportView.as_view(), name='import-xls'),
    url(r'^delete-inventory-return/$', DeleteInventoryReturnView.as_view(), name="delete-pro-in"),
    url(r'^delete-box/$', DeleteBoxView.as_view(), name="delete-box"),
    url(r'^delete-project/$', DeleteProjectView.as_view(), name="delete-project"),
    url(r'^delete-product/$', DeleteProductView.as_view(), name="delete-product"),
    url(r'^delete-model/$', DeleteModelView.as_view(), name="delete-model"),
    url(r'^delete-accessory/$', DeleteAccessoryView.as_view(), name="delete-accessory"),
    url(r'^delete-shelf/$', DeleteShelfView.as_view(), name="delete-shelf"),
    url(r'^delete-hole/$', DeleteHoleView.as_view(), name="delete-hole"),
    url(r'^delete-bom/$', DeleteFromBomListView.as_view(), name="delete-bom"),
    url(r'^delete-product-out/$', DeleteProductOutView.as_view(), name="delete-pro-out"),
    url(r'^delete-product-purchased/$', DeleteProductPurchasedView.as_view(), name="delete-pro-purchased"),
    url(r'^$', login_required(OverallInventoryView.as_view()), name="dashboard"),
]
