from django.contrib import admin
from import_export import resources, widgets
from import_export.fields import Field
from applications.accounts.models import User, Teams
from . models import Product, ProductOut, OverallInventory, ProductPurchase, BinLocation, InventoryReturned,\
    PurchaseRequest, Projects, Vendors, ProjectInventory, Innerholes, Shelves, ModelNames, Accessories, PurchaseOrder, \
    WorkWeek, Actuators, TopPlates, MachineSerialNumber, AccessoryDelivered
from import_export.admin import ImportExportModelAdmin


class ProductOutResource(resources.ModelResource):

    product = Field(
        column_name="product_out",
        attribute="product_out",
        widget=widgets.ForeignKeyWidget(Product, "part_description")
    )

    project = Field(
        column_name='project',
        attribute='project',
        widget=widgets.ForeignKeyWidget(Projects, 'name'))

    person = Field(
        column_name='person',
        attribute='person',
        widget=widgets.ForeignKeyWidget(User, 'first_name'))

    team = Field(
        column_name='team',
        attribute='team',
        widget=widgets.ForeignKeyWidget(Teams, 'name'))

    class Meta:
        model = ProductOut
        fields = ('return_count', 'out_count', 'recieved_date')


class ProductPurchaseResource(resources.ModelResource):

    product = Field(
        column_name="product_purchase",
        attribute="product_purchase",
        widget=widgets.ForeignKeyWidget(Product, "part_description")
    )

    purchaser = Field(
        column_name='purchaser',
        attribute='purchaser',
        widget=widgets.ForeignKeyWidget(User, 'first_name'))

    location = Field(
        column_name='location',
        attribute='location',
        widget=widgets.ForeignKeyWidget(Innerholes, 'name'))

    vendor = Field(
        column_name='vendor',
        attribute='vendor_name',
        widget=widgets.ForeignKeyWidget(Vendors, 'name'))

    class Meta:
        model = ProductPurchase
        fields = ('purchase_count', 'purchased_on',)


class ProductResource(resources.ModelResource):
    class Meta:
        model = Product
        fields = ('part_name', 'part_description', 'remarks')


class ProjectBom(resources.ModelResource):
    product = Field(
        column_name="inventory",
        attribute="inventory",
        widget=widgets.ForeignKeyWidget(Product, "part_description")
    )
    available = Field(
        column_name='available',
        attribute='available',
        widget=widgets.ForeignKeyWidget(OverallInventory, 'part_name'))

    project = Field(
        column_name='project',
        attribute='project',
        widget=widgets.ManyToManyWidget(Projects, field='name'))

    class Meta:
        model = ProjectInventory
        skip_unchanged = True
        report_skipped = True
        exclude = ('id',)
        import_id_fields = ('id',)
        fields = ('required_count',)


class ProductAdmin(ImportExportModelAdmin):
    resource_class = ProductResource


class ProjectAdmin(ImportExportModelAdmin):
    resource_class = ProjectBom


class ProductOutAdmin(ImportExportModelAdmin):
    resource_class = ProductOutResource


class ProductPurchaseAdmin(ImportExportModelAdmin):
    resource_class = ProductPurchaseResource


class PurchaseRequestAdmin(admin.ModelAdmin):
    exclude = ("stk_product_in", 'age', 'part_number',)


class MachineAdmin(admin.ModelAdmin):
    list_display = ["serial_no", "po_number", "get_destination"]

    def get_destination(self,obj):
        return obj.po_number.destination

    get_destination.short_description = "Destination"

admin.site.register(Product, ProductAdmin)
admin.site.register(OverallInventory)
admin.site.register(ProductPurchase, ProductPurchaseAdmin)
admin.site.register(ProductOut, ProductOutAdmin)
admin.site.register(InventoryReturned)
admin.site.register(PurchaseRequest, PurchaseRequestAdmin)
admin.site.register(BinLocation)
admin.site.register(Projects)
admin.site.register(ProjectInventory, ProjectAdmin)
admin.site.register(ModelNames)
admin.site.register(Accessories)
admin.site.register(AccessoryDelivered)
admin.site.register(PurchaseOrder)
admin.site.register(WorkWeek)
admin.site.register(TopPlates)
admin.site.register(Actuators)
admin.site.register(MachineSerialNumber, MachineAdmin)
admin.site.register(Vendors)
admin.site.register(Innerholes)
admin.site.register(Shelves)
