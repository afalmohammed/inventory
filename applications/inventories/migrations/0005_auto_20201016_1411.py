# Generated by Django 3.0.2 on 2020-10-16 08:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventories', '0004_auto_20201015_1948'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='overallinventory',
            name='remarks',
        ),
        migrations.AddField(
            model_name='product',
            name='remarks',
            field=models.TextField(blank=True, null=True),
        ),
    ]
