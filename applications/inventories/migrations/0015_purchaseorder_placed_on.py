# Generated by Django 3.0.2 on 2020-11-20 15:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventories', '0014_purchaseorder_total_delivered'),
    ]

    operations = [
        migrations.AddField(
            model_name='purchaseorder',
            name='placed_on',
            field=models.DateField(blank=True, null=True),
        ),
    ]
