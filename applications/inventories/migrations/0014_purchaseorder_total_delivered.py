# Generated by Django 3.0.2 on 2020-11-20 14:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventories', '0013_auto_20201120_1802'),
    ]

    operations = [
        migrations.AddField(
            model_name='purchaseorder',
            name='total_delivered',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
