import json
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import redirect
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views import View
from applications.accounts.models import User


class LoginView(View):

    def get(self, request):
        return render(self.request, 'login.html')

    def post(self, request):
        username = self.request.POST['username']
        password = self.request.POST['password']
        if username and password:
            user = authenticate(username=username, password=password)
        else:
            data = dict()
            data['result'] = "please fill both fields"
            return HttpResponse(json.dumps(data), content_type="application/json")
        if user:
            login(self.request, user)
            data = dict()
            data['result'] = "success"
            return HttpResponse(json.dumps(data), content_type="application/json")
        else:
            data = dict()
            data['result'] = "invalid credentials"
            return HttpResponse(json.dumps(data), content_type="application/json")


class SignUpView(View):

    def get(self, request):
        return render(self.request, 'signup.html')

    def post(self, request):
        if self.request.POST['first_name'] and self.request.POST['last_name'] and self.request.POST['uid'] \
                and self.request.POST['password']:
            user = User()
            user.first_name = self.request.POST['first_name']
            user.last_name = self.request.POST['last_name']
            # email = validateEmail(self.request.POST["email"])
            # if email == True:
            #     user.email = self.request.POST["email"].lower()
            #     user.username = user.email
            # else:
            #     data = dict()
            #     data['result'] = "Please enter a valid email address"
            #     return HttpResponse(json.dumps(data),
            #                         content_type="application/json")
            user.Uid = self.request.POST['uid']
            user.username = user.Uid
            user.email = user.Uid + "@ust-global.com"
            user.set_password(self.request.POST['password'])
            users = list(User.objects.values_list("Uid", flat=True))
            if user.Uid in users:
                data = dict()
                data['result'] = "User already exist.Please login"
                return HttpResponse(json.dumps(data), content_type="application/json")
            else:
                user.save()
                data = dict()
                data['result'] = "success"
                login(self.request, user)
                return HttpResponse(json.dumps(data),
                                    content_type="application/json")

        else:
            data = dict()
            data['result'] = "Both fields are mandatory"
            return HttpResponse(json.dumps(data),
                                content_type="application/json")


class LogoutView(View):

    @method_decorator(login_required)
    def get(self, request):
        logout(self.request)
        return redirect('/accounts/login/')




