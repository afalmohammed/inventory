from django.contrib.auth.models import AbstractUser
from django.db import models

class TimeStampModel(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


class Teams(TimeStampModel):
    name = models.CharField(max_length=256, blank=True, null=True)

    def __str__(self):
        return self.name


class User(AbstractUser, TimeStampModel):
    Uid = models.CharField(max_length=10, null=True, blank=True)
    purchaser = models.BooleanField(default=False)
    inventory_admin = models.BooleanField(default=False)
    team = models.ForeignKey(Teams, related_name="user_team", null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.username




